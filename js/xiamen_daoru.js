const requesturl="43.136.220.136:5000"  // 动态后端请求
const requesturlfront="43.136.220.136" // 静态资源加载
new Vue({
    el: '#vue-root', // 指定Vue根实例的挂载点
    data: {
        searchQuery: '', // 绑定到搜索输入框的数据
        searchResults: [], // 用来存储搜索结果的数组
        activeComponent: 'upload', // 默认显示上传组件
        currentPage: '当前光路上传', // 当前激活的页面
        inputContent: '', // 绑定到输入框的数据
        uniqueGuanglus: [], // 从Elasticsearch查询得到的唯一值数组
        showRecommendations: true, // 新增变量控制推荐列表的显示,
        selectedFile: null, // 保存选中的文件
        uploadFileName: '', // 由推荐项确定的上传文件名
    },
    computed: {
        // 根据输入内容过滤uniqueGuanglus数组
        filteredGuanglus() {
            // 修改逻辑，当showRecommendations为true时才显示推荐
            return this.showRecommendations && this.inputContent !== '' ? this.uniqueGuanglus.filter(item =>
                item.toLowerCase().includes(this.inputContent.toLowerCase())
            ) : [];
        }
    },watch: {
        inputContent(newVal) {
            // 当输入内容改变时，重新显示推荐列表
            if (newVal) {
                this.showRecommendations = true;
            }
        }
    },
    methods: {
        searchData() {
            // 示例查询Elasticsearch的逻辑，需要根据实际情况调整
            const esSearchQuery = {
                query: {
                    // 使用简单的match查询，实际情况可能需要更复杂的查询
                    match: {
                        _all: this.searchQuery
                    }
                },
                size: 10 // 假设我们只展示前10条结果
            };

            fetch(`${requesturl}/_search`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    // 需要根据实际情况提供认证信息
                },
                body: JSON.stringify(esSearchQuery)
            })
                .then(response => response.json())
                .then(data => {
                    this.searchResults = data.hits.hits.map(hit => hit._source);
                })
                .catch(error => console.error('Error searching:', error));
        },
        setActiveComponent(componentName) {
            this.activeComponent = componentName;
        },
        setActivePage(pageName) {
            this.currentPage = pageName; // 设置当前激活的页面
            // 在这里可以根据pageName来加载数据或进行其他操作
            // 例如：this.loadDataForPage(pageName);
        },
        setUniqueGuanglus(data) {
            this.uniqueGuanglus = data;
        },

        selectGuanglu(guanglu) {
            this.inputContent = guanglu; // 更新输入框内容为选择的推荐项
            this.showRecommendations = false; // 隐藏推荐列表
            this.uploadFileName = guanglu; // 更新待上传的文件名为选择的推荐项
        },
        handleFileUpload(event) {
            this.selectedFile = event.target.files[0]; // 保存选中的文件
        },
        uploadFile() {
            if (!this.selectedFile || !this.uploadFileName) {
                alert('请选择文件并确保已选择推荐项作为文件名！');
                return;
            }
            const formData = new FormData();
            formData.append('file', this.selectedFile, this.uploadFileName + '.xlsx'); // 设置上传的文件名
            fetch('http://localhost:4444/upload', {
                method: 'POST',
                body: formData,
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    alert('文件上传成功！');
                })
                .catch(error => {
                    console.error('Error uploading file:', error);
                    alert('文件上传失败！');
                });
        }

    },
    mounted() {
        // 定义Elasticsearch查询
        var esQuery = {
            size: 0, // 不需要具体的文档内容，只需要聚合结果
            aggs: {
                unique_guanglu: {
                    terms: {
                        field: "对应光路.keyword",
                        size: 10000 // 根据实际情况调整这个值以适应唯一值的数量
                    }
                }
            }
        };
        // 此处调用API获取uniqueGuanglus数据并设置
        fetch('http://43.136.220.136:9002/xianmen_guanglu/_search', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + btoa('admin:1992A1573')
            },
            body: JSON.stringify(esQuery)
        })
            .then(response => response.json())
            .then(data => {
                const uniqueGuanglus = data.aggregations.unique_guanglu.buckets.map(bucket => bucket.key);
                this.setUniqueGuanglus(uniqueGuanglus); // 设置uniqueGuanglus数组
            })
            .catch(error => console.error('Error fetching data:', error));


    //    所有数据查询

         esQuery = {
            query: {
                match_all: {} // 匹配所有文档
            },
            size: 100 // 控制返回的文档数量，这里假设返回前10条文档
        };

        fetch('http://43.136.220.136:9002/xianmen_guanglu/_search', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + btoa('admin:1992A1573') // 认证信息
            },
            body: JSON.stringify(esQuery)
        })
            .then(response => response.json())
            .then(data => {
                // 处理搜索结果
                // data.hits.hits数组包含了匹配的文档，每个文档的_source属性包含了文档的字段
                const searchResults = data.hits.hits.map(hit => hit._source);
                console.log(searchResults); // 输出或处理搜索结果
                // 这里你可以将searchResults赋值给Vue实例的某个数据属性，用于在界面上显示
            })
            .catch(error => console.error('Error fetching data:', error));


    }
});

// 其他js



// // 发送请求到Elasticsearch
// fetch('http://43.136.220.136:9002/xianmen_guanglu/_search', {
//     method: 'POST',
//     headers: {
//         'Content-Type': 'application/json',
//         'Authorization': 'Basic ' + btoa('admin:1992A1573') // 使用btoa进行base64编码
//     },
//     body: JSON.stringify(esQuery)
// })
//     .then(response => response.json())
//     .then(data => {
//         // 解析聚合结果，提取唯一值
//         const uniqueGuanglus = data.aggregations.unique_guanglu.buckets.map(bucket => bucket.key);
//         console.log(uniqueGuanglus); // 打印或处理唯一值数组
//     })
//     .catch(error => console.error('Error fetching data:', error));

