new Vue({
    el: '#appper',
    components: {
        QForm: Quasar.components.QForm,
        QSelect: Quasar.components.QSelect,
        QInput: Quasar.components.QInput,
        QTimeline: Quasar.components.QTimeline,
        QTimelineEntry: Quasar.components.QTimelineEntry,
        QBtn: Quasar.components.QBtn,
        QCard: Quasar.components.QCard,
        QCardSection: Quasar.components.QCardSection,
        QMarkdown: Quasar.components.QMarkdown,
        QAvatar: Quasar.components.QAvatar,
        VueMarkdown: window['VueMarkdown']
    },
    methods: {
// 复制消息内容
        copyMessage(index) {
            const messageContent = this.formItem.messages[index].content;
            const tempTextarea = document.createElement('textarea');
            tempTextarea.value = messageContent;
            document.body.appendChild(tempTextarea);
            tempTextarea.select();
            document.execCommand('copy');
            document.body.removeChild(tempTextarea);
            this.$q.notify({
                message: '内容已复制',
                color: 'positive',
            });
        },

        getUserAvatar() {
            // 从localStorage获取主题信息
            const themeStr = localStorage.getItem('zhuti');
            if (themeStr) {
                try {
                    const themeObj = JSON.parse(themeStr);
                    return themeObj.personalImg; // 返回头像URL
                } catch (e) {
                    console.error('解析主题信息失败:', e);
                }
            }
            return 'default_avatar.png'; // 如果没有获取到头像，则返回一个默认头像
        },
        renderMarkdown(markdownText) {
            const converter = new showdown.Converter({
                extensions: [],
                openLinksInNewWindow: true, // 如果需要，使链接在新窗口中打开
                simplifiedAutoLink: true,
                literalMidWordUnderscores: true,
                strikethrough: true,
                tables: true,
                tablesHeaderId: true,
                ghCodeBlocks: true,
                tasklists: true,
                smoothLivePreview: true,
                simpleLineBreaks: true,
                parseImgDimensions: true,
                emoji: true,
                underline: true,
                highlight: function (code, lang) {
                    return hljs.highlightAuto(code, [lang]).value;
                }
            });
            return converter.makeHtml(markdownText);
        },
        chooseColor(role) {
            switch (role) {
                case 'user': return 'blue';
                default: return 'green';
            }
        },
        clearDialog() {
            this.formItem.messages = [
                {
                    role: 'assistant',
                    content: '您好，请问有什么可以帮到您？',
                },
            ];
        },
        clearInput() {
            this.formItem.input = '';
        },
        onKeyDown(event) {
            if (event.ctrlKey && event.keyCode === 13) {
                this.submit();
            }
        },
        // 添加新消息到 messages 数组中
        addMessage(newMessage) {
            // 使用 $set 更新数组的特定索引
            this.$set(this.formItem.messages, this.formItem.messages.length, newMessage);
        },
        async submit() {


            const themeStr = localStorage.getItem('zhuti');
            let vipIsValid = true; // 默认会员有效

            if (themeStr) {
                try {
                    const themeObj = JSON.parse(themeStr);
                    if (themeObj.vipdate) {  // 检查vipdate属性是否存在
                        let vipDate = new Date(themeObj.vipdate);
                        if (!isNaN(vipDate.getTime())) {  // 检查vipDate是否为有效的日期
                            vipDate.setHours(0, 0, 0, 0);  // 清除时分秒毫秒
                            const today = new Date();
                            today.setHours(0, 0, 0, 0);  // 对当前日期也清除时分秒毫秒以确保公平比较
                            vipIsValid = vipDate >= today;
                        } else {
                            console.error('无效的vipdate:', themeObj.vipdate);
                            vipIsValid = false; // 如果vipdate无效，认为会员无效
                        }
                    }else{
                         console.error('无效的vipdate:', themeObj.vipdate);
                         vipIsValid = false; // 如果vipdate无效，认为会员无效
                    }
                } catch (e) {
                    console.error('解析主题信息失败:', e);
                }
            }else{
                         console.error('无效的vipdate:', themeObj.vipdate);
                         vipIsValid = false; // 如果vipdate无效，认为会员无效
                    }

            if (!vipIsValid) {
                Swal.fire({
                    icon: 'error',
                    title: '会员服务已到期或不可用',
                    text: '需要开通会员才能继续使用AI功能，请前往个人中心。',
                });
                return;
            }







            if (!this.formItem.input) {
                this.$q.notify({
                    message: '请输入',
                    color: 'negative',
                });
                return;
            } else {
                this.formItem.loading = true;
                this.addMessage({
                    role: 'user',
                    content: this.formItem.input,
                });
                //https://chatgpt.yinziming.com/api/openai/v1/chat/completions
                //https://worker-plain-glitter-b714.shikai327.workers.dev/api/openai/v1/chat/completions
                let apiEndpoint = 'https://chatgpt.yinziming.com/api/openai/v1/chat/completions'; // 指定的 API 端点
                //进行聊天开发，
                this.formItem.context = `你要知道你的角色，然后在回答中扮演这个角色，这是你这个角色的基础信息，你是人工智能助手kerly。当前是在抽认卡系统中，当前抽人卡集合名称为${localStorage.getItem('dangqianzhuti')}，
                这个集合所有的卡片内容的JSON字符串为${localStorage.getItem('previewCards')},当前进行中的卡片的JOSN字符串为${localStorage.getItem('currentCard')}，当前卡片在集合中的位置为${localStorage.getItem('currentIndex')}
                之后你要这些作为上下文回答之后的问题,不要提到JSON字符串，你应该说根据当前内容，然后回答问题，当前我的问题如下：`





                var chatArr = this.formItem.messages.slice(1, this.formItem.messages.length)
                chatArr.forEach(item => {
                    item.content = item.content.replaceAll(this.formItem.context, '');
                });

                var copiedArr = JSON.parse(JSON.stringify(chatArr));

                // 检查数组是否为空
                if (copiedArr.length > 0) {
                    // 获取数组的最后一个元素
                    let lastMessage = copiedArr[copiedArr.length - 1];

                    // 将 this.formItem.context 拼接到最后一个消息的内容前面
                    lastMessage.content = this.formItem.context + lastMessage.content;
                }

                // 准备请求选项
                const requestOptions = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'text/event-stream'  // 重要：指定期望的响应格式
                    },
                    body: JSON.stringify({
                        messages: copiedArr,
                        model: this.formItem.model
                    })
                };

                try {
                    const response = await fetch(apiEndpoint, requestOptions);
                    if (!response.ok) {
                        throw new Error('网络请求失败');
                    }

                    // 读取流式数据
                    const reader = response.body.getReader();
                    const decoder = new TextDecoder('utf-8');
                    let buffer = '';
                    const readStream = async () => {
                        var done1 = false
                        while (!done1) {
                            let { done, value } = await reader.read();

                            buffer =buffer+ decoder.decode(value, {stream: true});;
                            done1 = done
                        }
                        var chunk = buffer
                        console.log(chunk)
                        console.log(JSON.parse(chunk.replace(/data:/g, "")))
                        var content = JSON.parse(chunk.replace(/data:/g, "")).choices[0].message.content
                        console.log(content)
                        this.addMessage({
                            role: 'assistant',
                            content: content,
                        });
                        ({ done, value } = await reader.read());


                        reader.releaseLock();
                        this.formItem.loading = false;
                        console.log('Stream complete');
                    };

                    readStream();

                } catch (error) {
                    console.error("请求错误", error);
                    this.$q.notify({
                        message: '请求失败',
                        color: 'negative',
                    });
                    this.formItem.loading = false;
                }
            }
        }



    },
    data() {
        return {
            formItem: {
                messages: [
                    {
                        role: 'assistant',
                        content: '您好，请问有什么可以帮到您？',
                    },
                ],
                model: 'gpt-3.5-turbo',
                input: '',
                loading: false,
                context: '你要知道你的角色，然后在回答中扮演这个角色，这是你这个角色的基础信息，你是人工智能助手kerly。之后你要用这个回答之后的问题，当前我的问题如下：',
            },
            modelList: [
                'gpt-3.5-turbo',
                'gpt-4',
                'gpt-4-32k',
                'chatgml-6'
            ],
        };
    },
    mounted() {
        // 初始化或调用的代码
    }
});