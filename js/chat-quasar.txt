new Vue({
    el: '#appper',
    components: {
        QForm: Quasar.components.QForm,
        QSelect: Quasar.components.QSelect,
        QInput: Quasar.components.QInput,
        QTimeline: Quasar.components.QTimeline,
        QTimelineEntry: Quasar.components.QTimelineEntry,
        QBtn: Quasar.components.QBtn,
        QCard: Quasar.components.QCard,
        QCardSection: Quasar.components.QCardSection,
        QMarkdown: Quasar.components.QMarkdown,
        QAvatar: Quasar.components.QAvatar,
        VueMarkdown: window['VueMarkdown']
    },
    methods: {
        submitRecommendedQuestion(question) {
            this.formItem.input = question;
            this.submit();
        },
        loadChat(index) {
            this.formItem.messages = this.chats[index].messages;
            this.currentChatIndex = index
        },
        toggleFullScreen() {
            const elem = document.documentElement;
            if (!document.fullscreenElement) {
                if (elem.requestFullscreen) {
                    elem.requestFullscreen();
                } else if (elem.mozRequestFullScreen) { // Firefox
                    elem.mozRequestFullScreen();
                } else if (elem.webkitRequestFullscreen) { // Chrome, Safari and Opera
                    elem.webkitRequestFullscreen();
                } else if (elem.msRequestFullscreen) { // IE/Edge
                    elem.msRequestFullscreen();
                }
            } else {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.mozCancelFullScreen) { // Firefox
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) { // Chrome, Safari and Opera
                    document.webkitExitFullscreen();
                } else if (document.msExitFullscreen) { // IE/Edge
                    document.msExitFullscreen();
                }
            }
            this.menuQ = false; // 关闭菜单
        },
        toggleChatMenu(index) {
            // alert(this.chats[index].menu)


            this.$set(this.chats[index], 'menu', !this.chats[index].menu);
        },
        copyMessage(index) {
            const messageContent = this.formItem.messages[index].content;
            const tempTextarea = document.createElement('textarea');
            tempTextarea.value = messageContent;
            document.body.appendChild(tempTextarea);
            tempTextarea.select();
            document.execCommand('copy');
            document.body.removeChild(tempTextarea);
            this.$q.notify({
                message: '内容已复制',
                color: 'positive',
            });
        },

        getUserAvatar() {
            // 从localStorage获取主题信息
            const themeStr = localStorage.getItem('zhuti');
            if (themeStr) {
                try {
                    const themeObj = JSON.parse(themeStr);
                    return themeObj.personalImg; // 返回头像URL
                } catch (e) {
                    console.error('解析主题信息失败:', e);
                }
            }
            return 'default_avatar.png'; // 如果没有获取到头像，则返回一个默认头像
        },
        renderMarkdown(markdownText) {
            const converter = new showdown.Converter({
                extensions: [],
                openLinksInNewWindow: true,
                simplifiedAutoLink: true,
                literalMidWordUnderscores: true,
                strikethrough: true,
                tables: true,
                tablesHeaderId: true,
                ghCodeBlocks: true,
                tasklists: true,
                smoothLivePreview: true,
                simpleLineBreaks: true,
                parseImgDimensions: true,
                emoji: true,
                underline: true,
                highlight: function (code, lang) {
                    return hljs.highlightAuto(code, [lang]).value;
                }
            });
            return converter.makeHtml(markdownText);
        },
        chooseColor(role) {
            switch (role) {
                case 'user': return 'blue';
                default: return 'green';
            }
        },
        clearDialog() {
            this.formItem.messages = [
                {
                    role: 'assistant',
                    content: '您好，请问有什么可以帮到您？',
                },
            ];
            this.saveCurrentChat();
        },
        clearInput() {
            this.formItem.input = '';
        },
        onKeyDown(event) {
            if (event.ctrlKey && event.keyCode === 13) {
                this.submit();
            }
        },

        addMessage(newMessage) {

            this.$set(this.formItem.messages, this.formItem.messages.length, newMessage);
        },
        async submit() {


            const themeStr = localStorage.getItem('zhuti');
            let vipIsValid = true; // 默认会员有效

            if (themeStr) {
                try {
                    const themeObj = JSON.parse(themeStr);
                    if (themeObj.vipdate) {  // 检查vipdate属性是否存在
                        let vipDate = new Date(themeObj.vipdate);
                        if (!isNaN(vipDate.getTime())) {  // 检查vipDate是否为有效的日期
                            vipDate.setHours(0, 0, 0, 0);  // 清除时分秒毫秒
                            const today = new Date();
                            today.setHours(0, 0, 0, 0);  // 对当前日期也清除时分秒毫秒以确保公平比较
                            vipIsValid = vipDate >= today;
                        } else {
                            console.error('无效的vipdate:', themeObj.vipdate);
                            vipIsValid = false; // 如果vipdate无效，认为会员无效
                        }
                    }else{
                        console.error('无效的vipdate:', themeObj.vipdate);
                        vipIsValid = false; // 如果vipdate无效，认为会员无效
                    }
                } catch (e) {
                    console.error('解析主题信息失败:', e);
                }
            }else{
                console.error('无效的vipdate:', themeObj.vipdate);
                vipIsValid = false; // 如果vipdate无效，认为会员无效
            }

            if (!vipIsValid) {
                Swal.fire({
                    icon: 'error',
                    title: '会员服务已到期或不可用',
                    text: '需要开通会员才能继续使用AI功能，请前往个人中心。',
                });
                return;
            }







            if (!this.formItem.input) {
                this.$q.notify({
                    message: '请输入',
                    color: 'negative',
                });
                return;
            } else {
                this.formItem.loading = true;
                this.addMessage({
                    role: 'user',
                    content: this.formItem.input,
                });
                //https://chatgpt.yinziming.com/api/openai/v1/chat/completions
                //https://worker-plain-glitter-b714.shikai327.workers.dev/api/openai/v1/chat/completions
                let apiEndpoint = 'https://chatgpt.yinziming.com/api/openai/v1/chat/completions'; // 指定的 API 端点
                //进行聊天开发，
                this.formItem.context = `你要知道你的角色，然后在回答中扮演这个角色，这是你这个角色的基础信息，你是人工智能助手kerly。当前是在抽认卡系统中，当前抽人卡集合名称为${localStorage.getItem('dangqianzhuti')}，
                这个集合所有的卡片内容的JSON字符串为${localStorage.getItem('previewCards')},当前进行中的卡片的JOSN字符串为${localStorage.getItem('currentCard')}，当前卡片在集合中的位置为${localStorage.getItem('currentIndex')}
                之后你要这些作为上下文回答之后的问题,不要提到JSON字符串，你应该说根据当前内容，然后回答问题，当前我的问题如下：`





                var chatArr = this.formItem.messages.slice(1, this.formItem.messages.length)
                chatArr.forEach(item => {
                    item.content = item.content.replaceAll(this.formItem.context, '');
                });

                var copiedArr = JSON.parse(JSON.stringify(chatArr));

                // 检查数组是否为空
                if (copiedArr.length > 0) {
                    // 获取数组的最后一个元素
                    let lastMessage = copiedArr[copiedArr.length - 1];

                    // 将 this.formItem.context 拼接到最后一个消息的内容前面
                    lastMessage.content = this.formItem.context + lastMessage.content;
                }

                // 准备请求选项
                const requestOptions = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'text/event-stream'  // 重要：指定期望的响应格式
                    },
                    body: JSON.stringify({
                        messages: copiedArr,
                        model: this.formItem.model
                    })
                };

                try {
                    const response = await fetch(apiEndpoint, requestOptions);
                    if (!response.ok) {
                        throw new Error('网络请求失败');
                    }

                    // 读取流式数据
                    const reader = response.body.getReader();
                    const decoder = new TextDecoder('utf-8');
                    let buffer = '';
                    const readStream = async () => {
                        var done1 = false
                        while (!done1) {
                            let { done, value } = await reader.read();

                            buffer =buffer+ decoder.decode(value, {stream: true});;
                            done1 = done
                        }
                        var chunk = buffer
                        console.log(chunk)
                        console.log(JSON.parse(chunk.replace(/data:/g, "")))
                        var content = JSON.parse(chunk.replace(/data:/g, "")).choices[0].message.content
                        console.log(content)
                        this.addMessage({
                            role: 'assistant',
                            content: content,
                        });
                        ({ done, value } = await reader.read());


                        reader.releaseLock();
                        this.formItem.loading = false;
                        console.log('Stream complete');
                        // 保存当前会话内容
                        this.saveCurrentChat();
                    };

                    readStream();

                } catch (error) {
                    console.error("请求错误", error);
                    this.$q.notify({
                        message: '请求失败',
                        color: 'negative',
                    });
                    this.formItem.loading = false;
                }
            }
        },
        saveCurrentChat() {
            const currentIndex = this.currentChatIndex;
            if (currentIndex > -1) {
                this.$set(this.chats, currentIndex, { title: this.chats[currentIndex].title, messages: [...this.formItem.messages] });
            }
            localStorage.setItem('chats', JSON.stringify(this.chats));
        },
        newChat() {
            this.$q.notify({
                message: '新对话创建',
                color: 'positive',
            });
            this.chats.unshift(
                { title: '新会话'+'-'+new Date().toLocaleString(), menu: false , messages:[{
                        role: 'assistant',
                        content: '您好，请问有什么可以帮到您？',
                    }]
                }

            );
            this.loadChat(0)
            localStorage.setItem('chats', JSON.stringify(this.chats));
        },
        clearChat() {
            this.$q.notify({
                message: '所有对话已清理',
                color: 'positive',
            });
            this.chats = [];
            if(this.chats.length===0){
                this.chats.unshift({ title: '新会话'+new Date().toLocaleString(), menu: false , messages:[{
                        role: 'assistant',
                        content: '您好，请问有什么可以帮到您？',
                    }]
                })
            }
            this.loadChat(0)
            // 删除之后
            localStorage.setItem('chats', JSON.stringify(this.chats));


        },
        customizeTitle(chat) {
            this.$q.dialog({
                title: '自定义标题',
                prompt: {
                    model: '',
                    type: 'text'
                },
                cancel: true,
                persistent: true
            }).onOk(data => {
                chat.title = data;
                this.saveCurrentChat()
            });
        },
        deleteChat(index) {

            if (index > -1) {
                this.chats.splice(index, 1);
            }
            this.$q.notify({
                message: '会话已删除',
                color: 'positive',
            });
            if(this.chats.length===0){
                this.chats.unshift({ title: '新会话'+new Date().toLocaleString(), menu: false , messages:[{
                        role: 'assistant',
                        content: '您好，请问有什么可以帮到您？',
                    }]
                })
            }
            this.loadChat(0)
            // 删除之后
            localStorage.setItem('chats', JSON.stringify(this.chats));
        }
    },
    data() {
        return {
            recommendedQuestions: [
                '石锐博士最新研究进展？石锐博士最新研究进展？石锐博士最新研究进展？石锐博士最新研究进展？石锐博士最新研究进展？',
                '信息中心主要职责有哪些？',
                '如何联系石锐博士？'
            ],
            currentChatIndex:0,
            menuQ:false,
            formItem: {
                messages: [
                    {
                        role: 'assistant',
                        content: '您好，请问有什么可以帮到您？',
                    }
                ],
                model: 'gpt-3.5-turbo',
                input: '',
                loading: false,
                context: '你要知道你的角色，然后在回答中扮演这个角色，这是你这个角色的基础信息，你是人工智能助手kerly。之后你要用这个回答之后的问题，当前我的问题如下：',
            },
            modelList: [
                'gpt-3.5-turbo',
                'gpt-4',
                'gpt-4-32k',
                'chatgml-6'
            ],
            drawer: false,
            menu: false,
            chatMenu: false,
            chats: [
                // { title: '新会话', menu: false , messages:[{
                //         role: 'assistant',
                //         content: '您好，请问有什么可以帮到您？',
                //     }]}
            ]
        };
    },
    mounted() {
        // 初始化或调用的代码
        // this.chats.push({ title: '新会话', menu: false , messages:[{
        //         role: 'assistant',
        //         content: '您好，请问有什么可以帮到您？',
        //     }]})


        const savedChats = localStorage.getItem('chats');
        if (savedChats) {
            this.chats = JSON.parse(savedChats);
        } else {
            this.newChat()
        }

        if(this.chats.length===0){
            this.chats.unshift({ title: '新会话'+new Date().toLocaleString(), menu: false , messages:[{
                    role: 'assistant',
                    content: '您好，请问有什么可以帮到您？',
                }]
            })
        }
        // 如果有已经存在的会话，加载第一个会话的内容

        if (this.chats.length > 0) {
            this.formItem.messages = this.chats[0].messages;
        } else {
            this.formItem.messages = [];
        }
    }
});